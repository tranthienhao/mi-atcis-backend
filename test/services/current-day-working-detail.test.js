const assert = require('assert');
const app = require('../../src/app');

describe('\'current day working detail\' service', () => {
  it('registered the service', () => {
    const service = app.service('current-day-working-detail');

    assert.ok(service, 'Registered the service');
  });
});
