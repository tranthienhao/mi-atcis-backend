const assert = require('assert');
const app = require('../../src/app');

describe('\'working_detail\' service', () => {
  it('registered the service', () => {
    const service = app.service('working-detail');

    assert.ok(service, 'Registered the service');
  });
});
