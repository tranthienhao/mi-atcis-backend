const assert = require('assert');
const app = require('../../src/app');

describe('\'user_tag\' service', () => {
  it('registered the service', () => {
    const service = app.service('user-tag');

    assert.ok(service, 'Registered the service');
  });
});
