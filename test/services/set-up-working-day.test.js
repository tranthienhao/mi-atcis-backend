const assert = require('assert');
const app = require('../../src/app');

describe('\'set up working day\' service', () => {
  it('registered the service', () => {
    const service = app.service('set-up-working-day');

    assert.ok(service, 'Registered the service');
  });
});
