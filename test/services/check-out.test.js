const assert = require('assert');
const app = require('../../src/app');

describe('\'check out\' service', () => {
  it('registered the service', () => {
    const service = app.service('check-out');

    assert.ok(service, 'Registered the service');
  });
});
