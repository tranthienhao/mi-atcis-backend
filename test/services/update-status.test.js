const assert = require('assert');
const app = require('../../src/app');

describe('\'update status\' service', () => {
  it('registered the service', () => {
    const service = app.service('update-status');

    assert.ok(service, 'Registered the service');
  });
});
