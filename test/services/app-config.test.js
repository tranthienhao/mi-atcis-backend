const assert = require('assert');
const app = require('../../src/app');

describe('\'app config\' service', () => {
  it('registered the service', () => {
    const service = app.service('app-config');

    assert.ok(service, 'Registered the service');
  });
});
