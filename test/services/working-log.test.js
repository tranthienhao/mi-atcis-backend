const assert = require('assert');
const app = require('../../src/app');

describe('\'working log\' service', () => {
  it('registered the service', () => {
    const service = app.service('working-log');

    assert.ok(service, 'Registered the service');
  });
});
