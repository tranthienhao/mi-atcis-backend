const assert = require('assert');
const app = require('../../src/app');

describe('\'working_day\' service', () => {
  it('registered the service', () => {
    const service = app.service('working-day');

    assert.ok(service, 'Registered the service');
  });
});
