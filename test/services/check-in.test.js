const assert = require('assert');
const app = require('../../src/app');

describe('\'check in\' service', () => {
  it('registered the service', () => {
    const service = app.service('check-in');

    assert.ok(service, 'Registered the service');
  });
});
