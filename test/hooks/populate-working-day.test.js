const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const populateWorkingDay = require('../../src/hooks/populate-working-day');

describe('\'populate working day\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
