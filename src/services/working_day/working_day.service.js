// Initializes the `working_day` service on path `/working-day`
const createService = require('feathers-sequelize');
const createModel = require('../../models/working_day.model');
const hooks = require('./working_day.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  let paginate = {};
  paginate.max = 365;
  paginate.default = 365;
  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/working-day', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('working-day');

  service.hooks(hooks);
};
