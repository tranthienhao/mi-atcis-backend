// Initializes the `set up working day` service on path `/set-up-working-day`
const createService = require('./set-up-working-day.class.js');
const hooks = require('./set-up-working-day.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/set-up-working-day', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('set-up-working-day');

  service.hooks(hooks);
};
