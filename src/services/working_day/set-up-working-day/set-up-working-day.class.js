/* eslint-disable no-unused-vars */
const moment = require("moment");

class Service {
  constructor(options) {
    this.options = options || {};
    this.app = options.app;
  }

  async create(data, params) {
    const workingDayService = this.app.service("working-day");
    const currentDay = moment().format("YYYY-MM-DD"); // get current day
    let tempDay = currentDay;
    while (
      moment(tempDay).date() !== 1 ||
      moment(tempDay).month() !== 0 // month() return month - 1
    ) {
      if (
        moment(tempDay).format("dddd") !== "Saturday" &&
        moment(tempDay).format("dddd") !== "Sunday"
      ) {
        //add working day
        try {
          await workingDayService.create({ id: tempDay }, params);
        } catch {
          console.log("Duplicated day: " + moment(tempDay).format("YYYY-MM-DD"))
        }
      }
      tempDay = moment(tempDay).add(1, "days");
    }
    return { result: 0, code: `Set up success` };
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
