// Initializes the `upload image` service on path `/upload-image`
const hooks = require("./upload-image.hooks");
// feathers-blob service
const blobService = require("feathers-blob");
const fs = require("fs-blob-store");
const blobStorage = fs(__dirname + "/../../../public/images");
const multer = require("multer");
const multipartMiddleware = multer();

module.exports = function(app) {

  // Initialize our service with any options it requires
  app.use(
    "/upload-image",
    multipartMiddleware.single("uri"),
    function(req, res, next) {
      req.feathers.file = req.file;
      next();
    },
    blobService({ Model: blobStorage })
  );

  app.use("/images/:id", (req, res) => {
    res.sendFile(__dirname+"/../../../public/images" + req.params.id)
  })

  // Get our initialized service so that we can register hooks
  const service = app.service("upload-image");

  service.hooks(hooks);
};
