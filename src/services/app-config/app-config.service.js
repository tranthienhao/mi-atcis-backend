// Initializes the `app config` service on path `/app-config`
const createService = require('feathers-sequelize');
const createModel = require('../../models/app-config.model');
const hooks = require('./app-config.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/app-config', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('app-config');

  service.hooks(hooks);
};
