/* eslint-disable no-unused-vars */
const moment = require("moment");
const errors = require("feathers-errors");

class Service {
  constructor(options) {
    this.options = options || {};
    this.app = options.app;
  }

  create(data, params) {
    const currentDay = moment().format("YYYY-MM-DD"); // Get current day
    const workingDetailService = this.app.service("working-detail");
    const workingLogService = this.app.service("working-log");
    params.query.working_day_id = currentDay;
    return Promise.resolve(
      workingDetailService.find(params).then(result => {
        if (result.total !== 0) {
          if (result.data[0].status === "Present") {
            //update working details
            const updateTime = Date.now();
            const log_time = updateTime - result.data[0].updatedAt;
            const total_time = result.data[0].total_time + log_time;
            return Promise.resolve(
              workingDetailService
                .patch(
                  null,
                  { status: "Absent", total_time: total_time },
                  params
                )
                .then(() => {
                  const workingLogData = {
                    user_id: params.query.user_id,
                    working_day_id: currentDay,
                    activity: "Check Out",
                    log_time: log_time
                  };
                  return workingLogService.create(workingLogData, params);
                })
            );
          } else throw new errors.BadRequest({ message: "Check In Require." });
        } else {
          // show error code
          throw new errors.BadRequest({ message: "Check In Require." });
        }
      })
    );
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
