// Initializes the `check out` service on path `/check-out`
const createService = require('./check-out.class.js');
const hooks = require('./check-out.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/check-out', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('check-out');

  service.hooks(hooks);
};
