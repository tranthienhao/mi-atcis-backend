const { authenticate } = require("@feathersjs/authentication").hooks;
const getCompanyStatistic = require("../../../hooks/get-company-statistic");
const getUserStatistic = require("../../../hooks/get-user-statistic");

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [getCompanyStatistic()],
    get: [getUserStatistic()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
