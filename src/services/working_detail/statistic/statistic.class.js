/* eslint-disable no-unused-vars */
const moment = require("moment");
class Service {
  constructor(options) {
    this.options = options || {};
    this.app = options.app;
  }

  async find(params) {
    let result = { month: params.query.month };
    const workingDayService = this.app.service("working-day");
    const userService = this.app.service("users");
    await workingDayService
      .find({
        query: {
          $limit: 0,
          id: {
            $gte: moment(params.query.month).startOf("month"),
            $lte: moment(params.query.month).endOf("month")
          }
        }
      })
      .then(data => {
        result.total_working_day = data.total;
        result.total_day_off =
          moment(params.query.month).daysInMonth() - data.total;
      });
    //TODO: skip when have many user
    await userService.find({}).then(data => {
      result.users_statistic = data.data
    });
    return result;
  }

  async get(id, params) {
    let result = { month: params.query.month };
    const workingDayService = this.app.service("working-day");
    const userService = this.app.service("users");
    await workingDayService
      .find({
        query: {
          $limit: 0,
          id: {
            $gte: moment(params.query.month).startOf("month"),
            $lte: moment(params.query.month).endOf("month")
          }
        }
      })
      .then(data => {
        result.total_working_day = data.total;
        result.total_day_off =
          moment(params.query.month).daysInMonth() - data.total;
      });
    await userService.get(id).then(data => {
      result.users_statistic = data
    });
    return result;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
