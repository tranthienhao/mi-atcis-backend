// Initializes the `working_detail` service on path `/working-detail`
const createService = require('feathers-sequelize');
const createModel = require('../../models/working_detail.model');
const hooks = require('./working_detail.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    id: 'user_id',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/working-detail', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('working-detail');

  service.hooks(hooks);
};
