// Initializes the `update status` service on path `/update-status`
const createService = require('./update-status.class.js');
const hooks = require('./update-status.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/update-status', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('update-status');

  service.hooks(hooks);
};
