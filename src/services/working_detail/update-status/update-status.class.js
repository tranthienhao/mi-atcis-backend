/* eslint-disable no-unused-vars */
const moment = require("moment");

class Service {
  constructor(options) {
    this.options = options || {};
    this.app = options.app;
  }

  create(data, params) {
    const currentDay = moment().format("YYYY-MM-DD"); // Get current day
    const workingDetailService = this.app.service("working-detail");
    params.query.working_day_id = currentDay;
    return Promise.resolve(
      workingDetailService.find(params).then(result => {
        if (result.total !== 0) {
          // update working details
          return workingDetailService.patch(
            null,
            { status: data.status, total_time: 0 },
            params
          );
        } else {
          // create working details
          return workingDetailService.create(
            {
              user_id: params.query.user_id,
              working_day_id: currentDay,
              status: data.status
            },
            params
          );
        }
      })
    );
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
