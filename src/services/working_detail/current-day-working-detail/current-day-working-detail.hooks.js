const { authenticate } = require('@feathersjs/authentication').hooks;
const populateWorkingDetail = require('../../../hooks/populate-working-detail');
const populateWorkingDay = require('../../../hooks/populate-working-day');
const groupByStatus = require('../../../hooks/group-by-status');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [populateWorkingDetail(), groupByStatus()],
    get: [populateWorkingDetail(), populateWorkingDay()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
