// Initializes the `current day working detail` service on path `/current-day-working-detail`
const createService = require('./current-day-working-detail.class.js');
const hooks = require('./current-day-working-detail.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/current-day-working-detail', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('current-day-working-detail');

  service.hooks(hooks);
};
