/* eslint-disable no-unused-vars */
const moment = require("moment");

class Service {
  constructor(options) {
    this.options = options || {};
    this.app = options.app;
  }

  async find(params) {
    const userService = this.app.service("users");
    return Promise.resolve(userService.find(params));
  }

  async get(id, params) {
    const userService = this.app.service("users");
    return Promise.resolve(userService.get(id, params));
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
