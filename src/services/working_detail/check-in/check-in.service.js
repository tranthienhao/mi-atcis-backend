// Initializes the `check in` service on path `/check-in`
const createService = require('./check-in.class.js');
const hooks = require('./check-in.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/check-in', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('check-in');

  service.hooks(hooks);
};
