/* eslint-disable no-unused-vars */
const moment = require("moment");
const errors = require("feathers-errors");
class Service {
  constructor(options) {
    this.options = options || {};
    this.app = options.app;
  }

  create(data, params) {
    //TODO: Get wifi and location from app config and compare before check in
    const currentDay = moment().format("YYYY-MM-DD"); // Get current day
    const workingDetailService = this.app.service("working-detail");
    const workingLogService = this.app.service("working-log");
    params.query.working_day_id = currentDay;
    return Promise.resolve(
      workingDetailService.find(params).then(data => {
        if (data.total !== 0) {
          if (data.data[0].status !== "Present") {
            //update working details
            return Promise.resolve(
              workingDetailService
                .patch(null, { status: "Present" }, params)
                .then(() => {
                  const workingLogData = {
                    user_id: params.query.user_id,
                    working_day_id: currentDay,
                    activity: "Check In",
                    log_time: 0
                  };
                  return workingLogService.create(workingLogData, params);
                })
            );
          } else
            throw new errors.BadRequest({
              message: "User have checked in already."
            });
        } else {
          // create new working details
          const workingLogData = {
            user_id: params.query.user_id,
            working_day_id: currentDay,
            status: "Present",
            activity: "Check In",
            log_time: 0,
            total_time: 0
          };
          //TODO: get working time from app config to compare with current time 
          if (moment().isAfter(moment("09:00:00", "hh:mm:ss"))) {
            //user is checkin late
            workingLogData.is_late = true
          }
          return Promise.resolve(
            workingDetailService.create(workingLogData, params)
            .then(() => {
              return workingLogService.create(workingLogData, params);
            })
          );
        }
      })
    );
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
