// Initializes the `working log` service on path `/working-log`
const createService = require('feathers-sequelize');
const createModel = require('../../models/working-log.model');
const hooks = require('./working-log.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/working-log', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('working-log');

  service.hooks(hooks);
};
