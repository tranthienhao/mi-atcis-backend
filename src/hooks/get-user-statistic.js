// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars'
const moment = require("moment");
module.exports = function(options = {}) {
  return async context => {
    const { app, method, result } = context;
    const users =
      method === "find" ? result.users_statistic : [result.users_statistic];
    const month = result.month;
    const total_working_day = result.total_working_day;
    const workingDetailService = app.service("working-detail");

    let workingDayQuery = {
      $gte: moment(month).startOf("month"),
      $lte: moment(month).endOf("month")
    };
    await Promise.all(
      users.map(async item => {
        delete item.password;
        //get present day by user_id: find record which total_time > 0
        await workingDetailService
          .find({
            query: {
              $select: [ 'working_day_id', 'total_time', 'is_late' ],
              $limit: total_working_day,
              user_id: item.id,
              working_day_id: workingDayQuery,
              total_time: {
                $gt: 0
              }
            }
          })
          .then(data => {
            item.total_present_day = data.data.length;
            result.present_day = data.data  
          });
        //get excuted absent day by user_id
        await workingDetailService
          .find({
            query: {
              $select: [ 'working_day_id' ],
              $limit: total_working_day,
              user_id: item.id,
              working_day_id: workingDayQuery,
              status: "Excused Absence"
            }
          })
          .then(data => {
            item.total_excused_absent_day = data.data.length;
            result.excused_absent_day = data.data  
          });
        //get non excuted absent day by user_id
        if (moment().isSame(moment(month), "month")) {
          //if get statis by current month
          await app.service("working-day")
            .find({
              query: {
                $limit: 0,
                id: {
                  $gt: moment(),
                  $lte: moment(month).endOf("month")
                } 
              }
            })
            .then(data => {
              item.total_non_excuted_absent_day =
              total_working_day -
              item.total_present_day -
              item.total_excused_absent_day - 
              data.total
            });
        } else {
          item.total_non_excuted_absent_day =
            total_working_day -
            item.total_present_day -
            item.total_excused_absent_day;
        }

        //get total late day by user_id
        await app.service("working-log")
          .find({
            query: {
              $select: [ 'working_day_id', 'createdAt' ],
              $limit: total_working_day,
              user_id: item.id,
              working_day_id: workingDayQuery,
              is_late: 1
            }
          })
          .then(data => {
            item.total_late_day = data.data.length;
            result.late_day = data.data 
          });
      })
    );
    return context;
  };
};
