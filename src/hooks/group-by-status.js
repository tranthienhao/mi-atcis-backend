// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    const { app, method, result, params } = context;
    const working_detail = method === "find" ? result.data : [result];
    const data = { present: [], absent: [], others: [] };
    working_detail.forEach(item => {
      switch (item.status) {
        case "Present":
          data.present = [...data.present, item];
          break;
        case "Absent":
          data.absent = [...data.absent, item];
          break;
        default:
          data.others = [...data.others, item];
          break;
      }
    });
    result.data = data;
    return context;
  };
};
