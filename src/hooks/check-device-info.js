// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const errors = require("feathers-errors");
// eslint-disable-next-line no-unused-vars
module.exports = function(options = {}) {
  return async context => {
    const { app, data, params } = context;
    const user = params.user;
    if (data.device_id) {
      if (!user.device_id) {
        const userService = app.service("users");
        params.user.device_id = data.device_id;
        await userService.patch(user.id, { device_id: data.device_id });
      } else {
        if (user.device_id !== data.device_id) {
          //Wrong Device
          throw new errors.NotAcceptable({ message: "Wrong Device" });
        }
      }
    }
    return context;
  };
};
