// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
const errors = require("feathers-errors");
module.exports = function (options = {}) {
  return async context => {
    //check mac address
    console.log(context.data)
    //TODO: Get mac ip from app config table
    if(context.data.bssid !== "c8:3a:35:01:b7:c8"){
      throw new errors.NotAcceptable({ message: "Not in company" });
    }
    return context;
  };
};
