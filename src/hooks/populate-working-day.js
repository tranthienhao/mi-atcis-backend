// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
const moment = require("moment");

module.exports = function(options = {}) {
  return async context => {
    const { app, method, result, params } = context;
    const currentDay = moment().format("YYYY-MM-DD"); // Get current day
    params.query.id = currentDay;
    const users = method === "find" ? result.data : [result];
    await Promise.all(
      users.map(async item => {
        //get current working day
        var working_day = await app.service("working-day").find(params);
        if(working_day.total !== 0){
          item.working_day_note= working_day.data[0].note;
        }else{
          item.working_day_note= "Today is a day off!"
        }
      })
    );
    return context;
  };
};
