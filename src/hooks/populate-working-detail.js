// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
const moment = require("moment");

module.exports = function(options = {}) {
  return async context => {
    const { app, method, result } = context;
    const currentDay = moment().format("YYYY-MM-DD"); // Get current day
    const users = method === "find" ? result.data : [result];
    await Promise.all(
      users.map(async item => {
         //get working details by user_id
        var working_detail = await app.service("working-detail").find({query: {user_id: item.id, working_day_id: currentDay}});
        if(working_detail.total !== 0){
          item.updatedAt = working_detail.data[0].updatedAt;
          item.status = working_detail.data[0].status;
          item.total_time = working_detail.data[0].total_time;
          item.is_late = working_detail.data[0].is_late;
        }
        else{
          // users didn't check in will return Absent in status
          item.status = "Absent";
          item.total_time = 0;
          item.is_late = null
        }
      })
    );
    return context;
  };
};
