// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const workingLog = sequelizeClient.define('working_log', {
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'user',
        key: 'id',
      }
    },
    working_day_id: {
      type: DataTypes.DATEONLY,
      references: {
        model: 'working_day',
        key: 'id',
      }
    },
    activity: {
      type: DataTypes.STRING,
    },
    log_time:{
      type: DataTypes.INTEGER,
    },
    is_late: {
      type: DataTypes.BOOLEAN,
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  workingLog.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return workingLog;
};
