// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const appConfig = sequelizeClient.define('app_config', {
    macAddress: {
      primaryKey: true,
      type: DataTypes.STRING,
      allowNull: false
    },
    longitude:{
      type: DataTypes.STRING,
      allowNull: false
    },
    latitude:{
      type: DataTypes.STRING,
      allowNull: false
    },
    workingTime:{
      type: DataTypes.TIME,
      allowNull: false
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  appConfig.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return appConfig;
};
