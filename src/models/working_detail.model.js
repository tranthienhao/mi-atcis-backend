// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const workingDetail = sequelizeClient.define('working_detail', {
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      references: {
        model: 'user',
        key: 'id',
      }
    },
    working_day_id: {
      type: DataTypes.DATEONLY,
      primaryKey: true,
      references: {
        model: 'working_day',
        key: 'id',
      }
    },
    status: {
      type: DataTypes.STRING,
    },
    total_time: {
      type: DataTypes.INTEGER,
    },
    is_late: {
      type: DataTypes.BOOLEAN,
    },
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  workingDetail.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return workingDetail;
};
